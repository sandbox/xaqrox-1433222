SocialComments.networks.twitter.init = function() {
  jQuery.get('/social-comments/twitter/user', function (data) {
    if (data) {
      SocialComments.login('twitter', true);
    }
  });
}

SocialComments.networks.twitter.login = function () {
};

SocialComments.networks.twitter.logout = function () {
  jQuery.get('/social-comments/twitter/logout', function () {
    SocialComments.logout('twitter', true);
  });
};

SocialComments.networks.twitter.loggedIn = function () { 
  jQuery.get('/social-comments/twitter/user', function (data) {
    var user = jQuery.parseJSON(data);
    jQuery('#twitter-info').html('You\'re logged in as <a href="http://twitter.com/' + user.screen_name +'" target="_blank">@' + user.screen_name + '</a>. You can write as much as you want above, but on Twitter your comment will look like this:<div id="twitter-preview"><a href="">http://t.co/this-page</a></div>');
    SocialComments.networks.twitter.preview(); 
    jQuery('#edit-comment').keyup(SocialComments.networks.twitter.preview);
  });
};

SocialComments.networks.twitter.loggedOut = function () { 
  jQuery('#twitter-info').html('');
};

SocialComments.networks.twitter.post = function () {
  var post = {
    comment: jQuery('#social-comments-input-form #edit-comment').val(),
    path: jQuery('#social-comments-input-form input[name="path"]').val(),
  };
  jQuery.post('/social-comments/twitter/save', post, function (data) {
    SocialComments.display(data);
  });
};

SocialComments.networks.twitter.preview = function () {
  var comment = jQuery('#edit-comment').val();
  
  var mention = '@AskBigQs';
  var link_length = 20;
  var spaces = 2; 

  var free_chars = 140 - mention.length - link_length - spaces;
  if (comment.length > free_chars) {
    comment = comment.substr(0, free_chars - 3) + '...'
  }
  comment = '<a href="">@AskBigQs</a> ' + comment + ' <a href="">http://t.co/thispage</a>';

  jQuery('#twitter-preview').html(comment);
};
