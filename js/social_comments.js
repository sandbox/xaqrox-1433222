jQuery(document).ready( function () {
  SocialComments.reset();
  var inputs = jQuery('#social-actions input');
  inputs.click( function (e) {
    var elementId = jQuery(this).attr('id');

    if (elementId == 'twitter-login') {
      var ajax = jQuery.ajax({
        url: '/social-comments/twitter/oauth/auth_url',
        async: false,
      });
      SocialComments.spin();

      if (ajax.response) {
        var windowSpec = "left=500, top=500, height=400, width=600, location=no, menubar=no, status=no, toolbar=no, ";
        var authWindow = window.open(ajax.response, 'twitter_auth', windowSpec);
        // thank you, Oprah.
        var authWatcher = setInterval( function () {
          if (authWindow && authWindow.closed) {
            clearInterval(authWatcher);
            SocialComments.login('twitter', true);
          }
        }, 100); 
      }
      else {
        SocialComments.login('twitter', true);
      }
    }
    else {
      var idParts = elementId.split('-');
      var network = idParts[0];
      var action = idParts[1];
      SocialComments[action](network);
    }

    return false;
  });

  SocialComments.total = Number(jQuery('#social-comments-pager-form [name="total"]').val());

  jQuery('#social-comments-submit').click(function (e) {
    SocialComments.networks[SocialComments.currentNetwork].post();
    return false;
  });

  jQuery('#social-comments-spinner')
  .ajaxStart( function () {
    SocialComments.spin();
  })
  .ajaxStop( function () {
    SocialComments.unspin();
  });

  jQuery('.social-comments-load-more').click( function () {
    jQuery.get('/social-comments/page', {
      start : jQuery('.social-comments-comment').length,
      path : jQuery('#social-comments-pager-form [name="path"]').val(),
    },
    function (data) {
      if (data.length) {
        var newTotal = jQuery('.social-comments-comment').length + jQuery(data).filter('.social-comments-comment').length;
        if (newTotal == SocialComments.total) {
          jQuery('.social-comments-load-more').attr('disabled', 'disabled').hide();
        }
        jQuery('#social-comments').append(data).masonry('reload', {isAnimated : true});
      }
    });
    SocialComments.unspin();
    return false;
  });

});

var SocialComments = {
  networks: {
    facebook: {},
    twitter: {},
  },
};

SocialComments.init = function () {
  for (var network in SocialComments.networks) {
    SocialComments.networks[network].init();
  }
};

SocialComments.login = function (network, result) {
  if (result == null) {
    SocialComments.networks[network].login();
  }
  else {
    if (result == true) {
      SocialComments.currentNetwork = network;
      SocialComments.showActiveLogOut();
      SocialComments.hideInactive();
      jQuery('#social-comments-submit').removeClass('logged-out').addClass('logged-in').removeAttr('disabled');
      SocialComments.networks[network].loggedIn();
    }
  }
};

SocialComments.logout = function (network, result) {
  if (result == null) {
    SocialComments.networks[network].logout();
  }
  else {
    if (result == true) {
      SocialComments.reset();
      SocialComments.networks[network].loggedOut();
    }
  }
};

SocialComments.hideInactive = function () {
  for (var network in SocialComments.networks) {
    if (network != SocialComments.currentNetwork) {
      var selector = '#social-actions div[id^="' + network + '"][id$=actions]';
      jQuery(selector).hide();
    }
  }
};

SocialComments.showActiveLogOut = function () {
  jQuery('#' + SocialComments.currentNetwork + '-actions').show();
  jQuery('#' + SocialComments.currentNetwork + '-login').hide();
  jQuery('#' + SocialComments.currentNetwork + '-logout').show();
}

SocialComments.reset = function () {
  jQuery('#social-actions div[id$="info"]').empty();
  jQuery('#social-actions *').show();
  jQuery('#social-actions input[id$="logout"]').hide();
  jQuery('#social-comments-submit').removeClass('logged-in').addClass('logged-out').attr('disabled', 'disabled');
}

SocialComments.spin = function () {
  jQuery('#social-comments-spinner').show();
};

SocialComments.unspin = function () {
  jQuery('#social-comments-spinner').hide();
};

SocialComments.display = function (comment) {
  jQuery('#social-comments').prepend(comment);
  SocialComments.unspin();
  SocialComments.total += 1;
  jQuery('#social-comments').masonry('reload', {isAnimated: true});
}
