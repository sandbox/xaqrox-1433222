SocialComments.networks.facebook.init = function () {
  FB.getLoginStatus( function (response) {
    if (response.status === 'connected') {
      SocialComments.login('facebook', true);
    }
  });
};

SocialComments.networks.facebook.login = function () {
  SocialComments.spin();
  FB.login(
    function (response) {
      if (response.authResponse) {
        SocialComments.login('facebook', true);
      }
      else {
        SocialComments.unspin();
      }
    },
    {scope: 'publish_stream, read_stream'}
  );
};

SocialComments.networks.facebook.logout = function () {
  SocialComments.spin();
  FB.logout(function (response) {
    SocialComments.logout('facebook', true);
  });
};

SocialComments.networks.facebook.loggedIn = function () { 
  FB.api('/me', function (response) {
    var message = '<span>Logged in as </span>' +
      '<a href="http://facebook.com/' + response.id + '" target="_blank">' + response.name + '</a>.' +
      '<em> (Posts made here will be public on this page, ' +
      'but will use your default privacy settings on Facebook.)</em>';
    jQuery('#facebook-info').html(message);
    SocialComments.unspin();
  });
};

SocialComments.networks.facebook.loggedOut = function () { 
  jQuery('#facebook-info').html('');
  SocialComments.unspin();
};

SocialComments.networks.facebook.post = function () {
  var post = {
    link: window.location.href,
    message: jQuery('#social-comments-input-form #edit-comment').val(),
    privacy: { value: 'EVERYONE' },
  };
  FB.api('/me/feed', 'post', post, function (response) {
    if (!response || response.error) {
      alert('Ask Big Questions doesn\'t have permission to post on your wall. ');
    }
    else {
      FB.api(response.id, function (response) {
        response.path = jQuery('#social-comments-input-form input[name="path"]').val();
        jQuery.post(
          '/social-comments/facebook/save',
          { comment: response },
          function (data) {
            SocialComments.display(data);
          }
        );
      });
    }
  });
};
