<?php
/**
 * @file
 * Facebook-specific functions for the social_comments module.
 */

/**
 * Return a render element with a Facebook user's profile image.
 *
 * @return
 *   An associative array representing a Facebook user's profile image.
 *
 * @see social_comments_image()
 */
function social_comments_facebook_image($user_id) {
  return array(
    '#theme' => 'image',
    '#path' => 'http://graph.facebook.com/' . $user_id . '/picture',
  );
}

/**
 * Return a Facebook user's profile URL.
 *
 * @return
 *   A string representing a Facebook user's profile URL.
 *
 * @see social_comments_user_url()
 */
function social_comments_facebook_user_url($user_id) {
  return 'http://facebook.com/' . $user_id;
}

/**
 * Return the URL of a Facebook post.
 *
 * @return
 *   A string representing the URL of a Facebook post.
 *
 * @see social_comments_comment_url()
 */
function social_comments_facebook_comment_url($comment_id) {
  return 'http://facebook.com/' . str_replace('_', '/posts/', $comment_id);
}

/**
 * Given a Facebook post object in $_POST, store it in the db and print for display.
 */
function social_comments_facebook_comment_save() {
  $facebook_comment = $_POST['comment'];
  // If we don't have permission to see the post in question, the Graph API
  // return the string 'false'. 'false' evaluates to boolean TRUE. Just a reminder.
  if ($facebook_comment != 'false') {
    $comment_fields = array(
      'comment' => isset($facebook_comment['message']) ? $facebook_comment['message'] : NULL,
      'comment_id' => $facebook_comment['id'],
      'name' => $facebook_comment['from']['name'],
      'network' => 'facebook',
      'path' => $facebook_comment['path'],
      'time' => strtotime($facebook_comment['created_time']),
      'user_id' => $facebook_comment['from']['id'],
    );

    social_comments_display_new_comment($comment_fields);
  }
  else {
    print '<div class="social-comments-comment">We couldn\'t get your post from Facebook... Sorry!</div>';
    watchdog('social_comments', 'Error reading data from Facebook, response was:!response', array('!response' => print_r($facebook_comment)), WATCHDOG_DEBUG);
  }
}
