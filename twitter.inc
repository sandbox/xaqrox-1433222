<?php
/**
 * @file
 * Twitter-specific functions for the social_comments module.
 */

require_once libraries_get_path('twitteroauth') . '/twitteroauth/twitteroauth.php';

define('TWITTER_API_KEY', 'rPaRcefFThMr45UuPUMbtg');
define('TWITTER_API_SECRET', 'XBMlFaM2OcrWPZSJmSUPFRwqkyMCf0fMwUV2vPHo');

/**
 * Return a TwitterOAuth object in various states of authentication.
 *
 * @param $token
 *   (Optional) An associative array with keys 'token' and 'secret'.
 * @return
 *   A TwitterOAuth object.
 */
function social_comments_twitter($token = FALSE) {
  // First see if a token has been provided as an argument.
  // In practice this is the temporary credentials from the authentication
  // callback, or a stored access token from the second branch of this if.
  if ($token) {
    return new TwitterOAuth(TWITTER_API_KEY, TWITTER_API_SECRET, $token['token'], $token['secret']);
  }
  // If not, check to see if a token has already been stored in PHP's session.
  elseif (isset($_SESSION['token'])) {
    $token = array(
      'token' => $_SESSION['token']['oauth_token'],
      'secret' => $_SESSION['token']['oauth_token_secret'],
    );
    // Then return this function, with the stored token as an argument.
    return social_comments_twitter($token);
  }
  else {
    // If nothing else, return a TwitterOAuth object intialized with just the
    // client credentials.
    return new TwitterOAuth(TWITTER_API_KEY, TWITTER_API_SECRET);
  }
}

/**
 * Implements hook_menu(). Kind of.
 *
 * In the interest of rendering unto Twitter that which is Twitter's, this
 * function returns an associative array like that expected of hook_menu().
 * social_comments_menu() calls this function and glues these elements onto the
 * end of the array built there. Since these menu items expose Twitter-specific
 * functions all declared in this file, they are in this file too.
 *
 * @return
 *   An associative array of Drupal menu items.
 *
 * @see social_comments_menu()
 * @see js/twitter.js
 */
function social_comments_twitter_menu_callbacks() {
  $items['social-comments/twitter/oauth/auth_url'] = array(
    'page callback' => 'social_comments_twitter_oauth_url',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );

  $items['social-comments/twitter/oauth/redirect'] = array(
    'page callback' => 'social_comments_twitter_oauth_redirect',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );

  $items['social-comments/twitter/user'] = array(
    'page callback' => 'social_comments_twitter_user',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );

  $items['social-comments/twitter/logout'] = array(
    'page callback' => 'social_comments_twitter_logout',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );

  return $items;
}

/**
 * Print a URL for OAuth authentication.
 *
 * The numbered steps are from:
 * https://github.com/abraham/twitteroauth/blob/master/DOCUMENTATION
 */
function social_comments_twitter_oauth_url() {
  // If a session has already been initalized, social_comments_twitter() creates
  // a TwitterOAuth object using the stored access token, and doesn't like
  // building the URL.
  if (!isset($_SESSION['token'])) {
    // 1) Build TwitterOAuth object using client credentials.
    $twitter = social_comments_twitter();
    // 2) Request temporary credentials from Twitter.
    $callback = social_comments_get_url('social-comments/twitter/oauth/redirect');
    $request_token = $twitter->getRequestToken($callback);
    // 3) Build authorize URL for Twitter.
    $authorize_url = $twitter->getAuthorizeURL($request_token, TRUE);
    // 4) Redirect user to authorize URL.
    print $authorize_url;
  }
}

/**
 * Callback function after user has authenticated with Twitter.
 */
function social_comments_twitter_oauth_redirect() {
  // 6) Rebuild TwitterOAuth object with client credentials and temporary credentials.
  if (isset($_GET['oauth_token']) && isset($_GET['oauth_verifier'])) {
    print 'Authentication succeeded. This window will close automatically.';
    $temporary = array(
      'token' => $_GET['oauth_token'],
      'secret' => $_GET['oauth_verifier'],
    );
    $twitter = social_comments_twitter($temporary);
    // 7) Get token credentials from Twitter.
    $token = $twitter->getAccessToken();
    $_SESSION['token'] = $token;

    // Since this gets called in the auth popup, print some javascript to close
    // the window.
    drupal_static_reset('drupal_add_js');
    drupal_add_js('window.close();', 'inline');
    print drupal_get_js();
  }
  else {
    print 'Authentication failed. Or, you shouldn\'t be here.';
  }
}

/**
 * Print json-formatted Twitter user info if there is a stored token, or nothing.
 */
function social_comments_twitter_user() {
  if (isset($_SESSION['token'])) {
    $user = array(
      'user_id' => $_SESSION['token']['user_id'],
      'screen_name' => $_SESSION['token']['screen_name'],
    );
    print json_encode($user);
  }
}

/**
 * Notify Twitter that the session is over, and delete the stored access token.
 */
function social_comments_twitter_logout() {
  $twitter = social_comments_twitter();
  $twitter->post('account/end_session');
  unset($_SESSION['token']);
}

/**
 * Send a tweet to Twitter, store it in our database, and print a themed comment.
 */
function social_comments_twitter_comment_save() {
  $comment = $_POST['comment'];
  $path = $_POST['path'];

  // Tweet length minus lenght of the t.co URL minus 1 for a space between the
  // comment and the link equals the length to which we will trim the comment.
  $mention = '@AskBigQs';
  $link_length = variable_get('twitter_short_url_length');
  $spaces = 2; 

  $free_chars = 140 - strlen($mention) - $link_length - $spaces;
  if (strlen($comment) > $free_chars) {
    // If the comment gets clipped we actually clip 3 extra chars to leave room
    // for the ellipsis.
    $clipped = substr($comment, 0, $free_chars - 3) . '...';
    $tweet = $mention . ' ' . $clipped . ' ' . social_comments_get_url($path);
  }
  else {
    $tweet = $mention . ' ' . $comment . ' ' . social_comments_get_url($path);
  }
  
  // 8) Rebuild TwitterOAuth object with client credentials and token credentials.
  $twitter = social_comments_twitter();
  // 9) Query Twitter API.
  $data = array('status' => $tweet);
  $tweet = $twitter->post('statuses/update', $data);

  $comment_fields = array(
    'comment' => $comment,
    'comment_id' => $tweet->id_str,
    'name' => $tweet->user->screen_name,
    'network' => 'twitter',
    'path' => $path,
    'time' => strtotime($tweet->created_at),
    'user_id' => $tweet->user->screen_name,
  );

  social_comments_display_new_comment($comment_fields);
}

/**
 * Query Twitter for a user's profile pic url, and return an image element.
 *
 * @return
 *   An associative array representing a Twitter user's profile image.
 *
 * @see social_comments_image()
 */
function social_comments_twitter_image($user_id) {
  $twitter = social_comments_twitter();
  $image_url = $twitter->get('users/show', array('screen_name' => $user_id))->profile_image_url;
  return array(
    '#theme' => 'image',
    '#path' => $image_url,
  );
}

/**
 * Return a Twitter user's profile URL.
 *
 * @return
 *   A string representing a Twitter user's profile URL.
 *
 * @see social_comments_user_url()
 */
function social_comments_twitter_user_url($user_id) {
  return 'http://twitter.com/' . $user_id;
}

/**
 * Return the URL of a tweet.
 *
 * @return
 *   A string representing the URL of a tweet.
 *
 * @see social_comments_comment_url()
 */
function social_comments_twitter_comment_url($comment_id) {
  // Part of the URL is the user's screen name, so grab that from the DB.
  $query = "SELECT user_id FROM {social_comments} WHERE network='twitter' AND comment_id=:comment_id";
  $replace = array(':comment_id' => $comment_id);
  $user_id = db_query($query, $replace)->fetchField();
  return 'http://twitter.com/' . $user_id . '/status/' . $comment_id;
}

/**
 * Get the current short URL lenght from Twitter and store it in the variables table.
 *
 * @see social_comments_cron().
 */
function social_comments_twitter_cron() {
  $twitter = social_comments_twitter();
  $config = $twitter->get('help/configuration');
  variable_set('twitter_short_url_length', $config->short_url_length);
}
