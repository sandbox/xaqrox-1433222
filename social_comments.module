<?php
/**
 * @file
 * Integration and presentation of social network engagement.
 */

define('SOCIAL_COMMENTS_PER_PAGE', 4);

/**
 * Return an array of strings, one for each social network with an implementation.
 */
function social_comments_networks() {
  return array(
    'facebook',
    'twitter',
  );
}

foreach (social_comments_networks() as $network) {
  require_once($network . '.inc');
}

/**
 * Build a renderable array containing the comment form and comments from social networks.
 *
 * @return
 * A renderable array containing the comment form and comments from social networks.
 */
function social_comments_block() {
  global $base_url;
  $url_parts = parse_url($base_url);
  $domain_name = $url_parts['host'];
  drupal_add_js(array('socialComments' => array(
    'appId' => '648511645165591',
    'domainName' => $domain_name,
  )), 'setting');

  $content[] = drupal_get_form('social_comments_input_form');
  $content[] = social_comments_comments();
  $content[] = drupal_get_form('social_comments_pager_form');
  return $content;
}

/**
 * Implements hook_block_info().
 */
function social_comments_block_info() {
  return array(
    'block' => array(
      'info' => 'Social Comments',
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function social_comments_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'block':
      $block['subject'] = 'Add your voice';
      $block['content'] = social_comments_block();
      break;
  }
  return $block;
}

/**
 * Implements hook_menu().
 */
function social_comments_menu() {
  $items['social-comments/%social_comments_comment'] = array(
    'title' => 'Social Comments',
    'page callback' => 'social_comments_comment_view',
    'page arguments' => array(1),
    'access callback' => TRUE,
  );
  $items['social-comments/%social_comments_comment/view'] = array(
    'title' => 'View',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );
  $items['social-comments/%social_comments_comment/delete'] = array(
    'title' => 'Delete comment',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('social_comments_comment_delete_form', 1),
    'access callback' => TRUE,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );
  $items['social-comments/page'] = array(
    'page callback' => 'social_comments_page_display',
    'type' => MENU_CALLBACK,
    'access arguments' => array('access content'),
  );

  // Each network will have a callback that handles input from the user, posts
  // it to the network (if it hasn't already been), stores it in our database,
  // and prints out a themed comment for display.
  foreach (social_comments_networks() as $network) {
    $items['social-comments/' . $network . '/save'] = array(
      'page callback' => 'social_comments_' . $network . '_comment_save',
      'type' => MENU_CALLBACK,
      'access arguments' => array('access content'),
      'file' => $network . '.inc',
    );
  }
  
  // Include Twitter's callbacks.
  $items += social_comments_twitter_menu_callbacks(); 

  return $items;
}

/**
 * Build a renderable array of comments from social networks.
 *
 * @return
 * A renderable array of comments.
 */
function social_comments_comments() {
  $comments = social_comments_page();
  $comments['#prefix'] = '<div id="social-comments">';
  $comments['#suffix'] = '</div>';

  return $comments;
}

/**
 * Get a page of comments from the database and return a render element
 */
function social_comments_page($start=0, $path=FALSE) {
  $query = "SELECT * FROM {social_comments} WHERE path=:path ORDER BY time DESC";
  $replace = array(':path' => $path ? $path : current_path());
  $results = db_query_range($query, $start, SOCIAL_COMMENTS_PER_PAGE, $replace);
  $assoc = $results->fetchAllAssoc('cid');

  $comments = array();
  foreach ($assoc as $cid => $comment) {
    $comments[$cid] = social_comments_comment_view($comment, TRUE);
  }

  return $comments;
}

/**
 * AJAX callback to render a new page of comments.
 */
function social_comments_page_display() {
  if (isset($_GET['start']) && isset($_GET['path'])) {
    print drupal_render(social_comments_page($_GET['start'], $_GET['path']));
  }
}

/**
 * Form constructor for the pager form.
 *
 * @ingroup forms
 */
function social_comments_pager_form() {
  $form = array();

  // Get the total count for comments on this page.
  $query = 'SELECT count(*) FROM {social_comments} WHERE path=:path';
  $replace = array(':path' => current_path());
  $total_comments = db_query($query, $replace)->fetchField();

  // Only display the pager if needed.
  if ($total_comments > SOCIAL_COMMENTS_PER_PAGE) {
    $form['load'] = array(
      '#type' => 'button',
      '#value' => 'Load More',
      '#attributes' => array(
        'class' => array('social-comments-load-more'),
      ),
    );
    // Make the Drupal path and total comments count for this path available.
    $form['path'] = social_comments_path_element();
    $form['total'] = array(
      '#type' => 'hidden',
      '#value' => $total_comments,
    );
  }

  return $form;
}

/**
 * Insert a comment in the database.
 *
 * @param $comment
 *   An associative array with keys corresponding to fields in the social_comments table.
 * @return
 *   The id of the newly-inserted comment.
 */
function social_comments_comment_insert($comment) {
  return db_insert('social_comments')->fields($comment)->execute();
}

/**
 * Load a comment from the database.
 *
 * @param $cid
 *   The unique id of the comment to be loaded.
 * @return
 *   An object representing a comment.
 */
function social_comments_comment_load($cid) {
  $query = 'SELECT * FROM {social_comments} WHERE cid=:cid';
  $replace = array(':cid' => $cid);
  return db_query($query, $replace)->fetchObject();
}

/**
 * Create a renderable array representing a comment.
 *
 * @param $comment
 *   An object representing a comment.
 * @return
 *   A renderable array representing a comment.
 */
function social_comments_comment_view($comment, $links = FALSE) {
  $build = array(
    '#theme' => 'social_comments_comment',
    '#object' => $comment,
    '#weight' => time() - $comment->time,
    // current time - time of comment = elapsed time since comment
    // newer comment -> smaller elapsed time -> "lighter" weight -> displayed at top
  );
  if ($links) {
    $build['#contextual_links'] = array(
      'social_comments' => array(
        'social-comments',
        array($comment->cid),
      ),
    );
  }
  return $build;
}

/**
 * A helper function to display new comments.
 * 
 * @param $comment_fields
 *   An associative array as expected by InsertQuery::fields
 */
function social_comments_display_new_comment($comment_fields) {
  $cid = social_comments_comment_insert($comment_fields);
  $comment_obj = social_comments_comment_load($cid);
  $comment_arr = social_comments_comment_view($comment_obj, TRUE);
  print drupal_render($comment_arr);
}

/**
 * Delete a comment from the database.
 *
 * @param $comment
 *   An object representing a comment.
 */
function social_comments_comment_delete($comment) {
  db_delete('social_comments')->condition('cid', $comment->cid)->execute();
  drupal_set_message(t('Comment successfully deleted.'));
}


/**
 * Return the absolute URL for the given path, or if none is supplied, the current page.
 *
 * @return
 * The absolute URL for the given path, or if none is supplied, the current page.
 */
function social_comments_get_url($path = FALSE) {
  if (!$path) {
    $path = current_path();
  }
  return url($path, array('absolute' => TRUE));
}

/**
 * Form constructor for the input form.
 *
 * @ingroup forms
 */
function social_comments_input_form() {
  $networks = social_comments_networks();

  $js_path = drupal_get_path('module', 'social_comments') . '/js/';
  $form['#attached']['js'] = array(
    $js_path . 'social_comments.js',
  );
  foreach ($networks as $network) {
    $form['#attached']['js'][] = $js_path . $network . '.js';
  }

  $form['comment'] = array(
    '#type' => 'textarea',
    '#maxlength' => '140',
    '#attributes' => array(
      '#id' => 'social-comment',
    ),
  );

  $form['path'] = social_comments_path_element();
  
  $form['actions']['social'] = array(
    '#prefix' => '<div id="social-actions">',
    '#suffix' => '</div>',
  );

  foreach ($networks as $network) {
    $form['actions']['social'][$network] = array(
      '#prefix' => '<div id="' . $network . '-actions">',
      '#suffix' => '</div>',
    );
    $form['actions']['social'][$network]['info'] = array(
      '#markup' => '<div id="' . $network . '-info" class="social-info"></div>',
    );
    foreach (array('login' => 'Log in to ', 'logout' => 'Log out of ') as $action => $verb) {
      $form['actions']['social'][$network][$action] = array(
        '#prefix' => '<div class="' . $network . '-action">',
        '#suffix' => '</div>',
        '#attributes' => array(
          'id' => $network . '-' . $action,
          'class' => array(
            'social-' . $action,
          ),
        ),
        '#type' => 'button',
        '#value' => t($verb . ucfirst($network)),
      );
    }
  }

  $form['actions']['spinner'] = array(
    '#theme' => 'image',
    '#path' => drupal_get_path('module', 'social_comments') . '/img/ajax-loader.gif',
    '#attributes' => array(
      'id' => 'social-comments-spinner',
    ),
  );

  $form['actions']['submit'] = array(
    '#attributes' => array(
      'id' => 'social-comments-submit',
    ),
    '#type' => 'button',
    '#value' => t('Add your voice'),
  );

  return $form;
}

/**
 * Implements hook_theme().
 */
function social_comments_theme($existing, $type, $theme, $path) {
  global $base_url;
  $url_parts = parse_url($base_url);
  $domain_name = $url_parts['host'];
  return array(
    'social_comments_comment' => array(
      'render element' => 'element',
      'template' => 'social-comments-comment',
    ),
    'social_comments_facebook_init' => array(
      'variables' => array(
        'domain_name' => $domain_name,
      ),
      'template' => 'social-comments-facebook-init',
    ),
  );
}

function template_preprocess_social_comments_comment(&$variables) {
  $comment = $variables['element']['#object'];
  $variables += get_object_vars($comment);
  $variables['time'] = format_interval(time() - $comment->time, 1);
  $variables['image'] = social_comments_image($comment);
  $variables['user_url'] = social_comments_user_url($comment);
  $variables['comment_url'] = social_comments_comment_url($comment);
}

/**
 * Theme helper function to get a commenter's profile image from a social
 * network implementation.
 *
 * @return
 *   A renderable array representing a commenter's profile image.
 */
function social_comments_image($comment) {
  $image = 'social_comments_' . $comment->network . '_image';
  return $image($comment->user_id);
}

/**
 * Theme helper function to get a commenter's profile URL from a social
 * network implementation.
 *
 * @return
 *   A string representing a commenter's profile URL.
 */
function social_comments_user_url($comment) {
  $user_url = 'social_comments_' . $comment->network . '_user_url';
  return $user_url($comment->user_id);
}

/**
 * Theme helper function to get a comment URL from a social network implementation.
 *
 * @return
 *   A string comment URL.
 */
function social_comments_comment_url($comment) {
  $comment_url = 'social_comments_' . $comment->network . '_comment_url';
  return $comment_url($comment->comment_id);
}

/**
 * Form constructor for the comment delete form.
 *
 * @param $comment
 *   The comment to be deleted.
 *
 * @see social_comments_comment_delete_form_submit()
 *
 * @ingroup forms
 */
function social_comments_comment_delete_form($form, &$form_state, $comment) {
  $original_url = drupal_get_path_alias($comment->path);
  $form['info'] = array(
    // Display the comment in question.
    'comment' => social_comments_comment_view($comment),
    'from' => array(
      '#prefix' => '<div class="info">Are you sure you want to delete this comment from this page: ',
      '#type' => 'link',
      '#title' => $original_url,
      '#href' => $original_url,
      '#options' => array(
        'attributes' => array(),
        'html' => FALSE,
      ),
      '#suffix' => '?</div>',
    ),
  );
  // Keep the comment object in the form so it can get passed to
  // social_comments_comment_delete() on submit.
  $form['comment'] = array(
    '#type' => 'value',
    '#value' => $comment,
  );
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['delete'] = array(
    '#type' => 'submit',
    '#value' => 'Delete',
  );
  return $form;
}

/**
 * Form submission handler for social_comments_comment_delete_form().
 */
function social_comments_comment_delete_form_submit($form, &$form_state) {
  social_comments_comment_delete($form['comment']['#value']);
}

/**
 * Implements hook_cron().
 * 
 * Right now, just a helper function to let social network implementations do
 * cron-y things.
 */
function social_comments_cron() {
  foreach (social_comments_networks() as $network) {
    $cron = 'social_comments_' . $network . '_cron';
    if (function_exists($cron)) {
      $cron();
    }
  }
}

function social_comments_path_element() {
  return array(
    '#type' => 'hidden',
    '#value' => current_path(),
  );
}

/*
function social_comments_page_alter(&$page) {
  dpm($page);
  $weights = array();
  foreach ($page['page_top'] as $name => $element) {
    if (isset($element['#weight'])) {
      $weight = $element['#weight'] * 2;
      $page['page_top'][$name]['#weight'] = $weight;
      $weights[] = $weight;
    }
  }
  asort($weights);
  $lightest = $weights[0];

  $facebook_init = array(
    '#theme' => 'social_comments_facebook_init',
    '#weight' => $lightest - 2,
  );
  $page['page_top']['social_comments_facebook_init'] = $facebook_init;
  dpm($page);
}*/
