<?php
/**
 * @file
 * Displays a social comment.
 *
 * Available variables:
 * - $element: The comment object from the database.
 * - $name: The commenter's name.
 * - $network: The social network used to post the comment.
 * - $user_url: The URL of the commenter's profile on the network.
 * - $image: A image render element representing the commenter's profile image.
 * - $time: The time since the comment was posted in text, i.e "3 hours".
 * - $comment_url: The URL of the post on the network.
 * - $comment: The text of the comment.
 *
 * @see template_preprocess_social_comments_comment()
 *
 * @ingroup themeable
 */
?>
<div class="<?php print $classes ?>">
  <?php print render($title_prefix); ?>
  <?php print render($title_suffix); ?>
  <div class="inner">
    <div class="user-info">
      <div class="col-2">
        <div class="name">
          <?php print l($name, $user_url, array('attributes' => array('target' => '_blank'))); ?>
        </div>
      </div>
      <div class="col-2">
        <div class="network">via <?php print $network ?></div>
      </div>
    </div>
    <div class="image">
      <?php print l('<div class="overlay"></div>' . render($image), $user_url, array('html' => TRUE, 'attributes' => array('target' => '_blank'))); ?>
    </div>
    <div class="ago">
      <?php print l($time . ' ago', $comment_url, array('attributes' => array('target' => '_blank'))); ?>
    </div>
    <div class="comment"><?php print $comment ?></div>
  </div>
</div>
